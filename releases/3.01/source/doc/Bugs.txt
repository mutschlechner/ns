Legend
------
- = unaddressed
O = "fixed", not tested or not fully tested
X = not going to fix, no longer needed

  Bugs are listed in priority.


Bug list
--------
+ Don't allow aliens to pick up marine weapons and vice versa (also don't drop weapons when alien dies) (also only allow weapons for role)
+ Key bind problem (config.cfg gets rewritten incorrectly sometimes (with topdown bindings) )
+ Hitting ESC in top-down mode should do the right thing
O Shotgun (and other weapons?) fire their effects even when out of ammo
- All weapons should show up in HUD and be properly placed 
	- Alien weapons should show up in HUD 
	- Welder doesn't show up in HUD
	+ Incorrect sprites are drawing for weapons
- Change weapon artwork so weapons don't do crazy things
	+ Fix up machine gun, knife and shotgun animations so they aren't embarassing
	+ Mostly done, need to fix the knife idle anim though
	- Add better pics for alien weapons
O Init alert to not do anything before the first alert is set
- can't chat while in commander mode **
+ Research messages are going to all players
+ MG underpowered, alien lifeforms massively underpowered
O Remove blue line around hierarchy
O Don't let bots use a leavegame entity
O Turrets don't have nearly enough hitpoints
- Turret solidity still weird
O Siege should only target entities in it's LOS
- Command sayings should go to entire team...convert to event.  Convert PlayHUDSound to local event as well?
- Turrets come to life when you touch them, even when not built
O Hives are still solid after being set active
- Overwatch
O Players don't join hierarchy correctly
- Turrets should attack hives
O Hives should count as aliens for "enemy sighted" blinking
O Cloaked aliens should NOT count for "enemy sighted" blinking
O Turrets and siege turrets should not fire at cloaked aliens
- Welding status bar going to all players?
+ Alien weapons still don't fire correctly on net clients
	+ Weird event playback problem with spit gun: runfuncs isn't being set for local player.  Once the server pauses the game by alt-tabbing back to MSDEV for a couple secs, the problem is fixed?!
	+ It appears that this everything is playing correctly on other machines, just not locally
	+ Claws don't work anymore, same problem? (they're not even showing up in HUD)
- Allow scrolling from arrow keys
- Left to right in tech tree doesn't make sense
- Make logout button more obvious
- Progress bar for building
+ Victory conditions
	+ Killing all hives doesn't prevent aliens from respawning
	+ Ready room doors
	+ Don't allow marines to fire before game starts (allow aliens to though?)
- Jetpack is underpowered and acts weirdly sometimes
O Marine upgrades goes away at death
O Ammo being given out correctly at beginning of round?
O Shotgun isn't being spawned correctly
O Don't allow joining teams or going back to ready room after game has started
O When everyone leaves a server, the game doesn't reset
- When the game resets
	- Reset doors to the right position
	O Remove all dead bodies (actually, fade these out immediately on death)
	- Remove scorch marks
- Weapons aren't being enabled/disabled correctly when player moves between ready room and game (but client is...wait does this even make sense?)
- Investigate/remove impulse 101 and flashlight impulse
- When joining a server, it's not setting music to the right offset, it starts at beginning
- When typing disconnect, .mp3 playback is still going
O Reduce size of machine gun bullet smoke, so big it obscures and slows down when targets are close (or don't play when you hit non-world)
- Add "I'm in position", "Need assistance", "Need ammo", "I need some help here" sounds
O Don't allow commander to scroll before the game starts
+ Finalize system to allow aliens vs. aliens and humans vs. humans
O When a player leaves a team, remove him from the team hierarchy (this is why jumping back and forth between teams causes game to start, it thinks there are players on both teams)
+ Add bullet smoke puffs
- Armor isn't visible in HUD (is that it in the lower right corner?)
+ Crash when picking up ammo while holding knife
+ Fix dropping of alien weapons and knife
O Check welding
	O Fix progress bar for welds that are opened
	- Don't allow welding progress when any player is touching the weld
	- Fix solidity problem (check the HL code on "use" to see how it traces a non-solid, non-blocking, visible entity?) (FL_WORLDBRUSH?)
+ Add crosshairs (pfnSetCrosshair)
- Make sure aliens can't "use" things
- Fix up overwatch
+ Add in scoreboard of some kind
- Particle editing bug
- Fix crap text for tech buttons (use images)
- Fix flight events so it makes sound and plays animation if possible
- Revisit resource injections
	- Link resource variables to info_map_info
- Have alien hives give more feedback when taking damage
	- Blood?
	- Marines get "the enemy is under attack"
	O Hive screams or makes gushing noises when hit
	- Should show damage as status bar?
+ Reload sprites in alien pie menu on map switch (is this causing the crash?  this seems to be the only map switching bug right now!)
- Observer mode doesn't look up or down (also make player invisible for rider)
- Observer mode won't let you change targets correctly
- Add entities that observer starts at so you don't have to travel to the map
- Clip observer to the map extents and view height
- Clicking the mouse in top-down mode moves
+ When there are no people in the game, reset it then set flag indicating the game has been reset.  When a player joins, reset flag again
O When going back to ready room, remove all upgrades and reset player model render mode (could've been invisible)
+ Only send alert when player takes damage from enemy, not from falling
+ Alien regenerating after death?
+ When connecting to existing server, particles aren't being triggered for creation
O Don't allow "logout" button to function after a drag (like tech buttons)
- Welder, mines, weapons aren't spawning when placed in map from Worldcraft (also, rename all weapons to weapon_)
O Major performance issues (largely vguiDrawLine, because filling rect proved VERY slow when drawing minimap with it)
- When going into top-down mode for like the 10th time, viewpoint is totally wrong (?)
O Setting gamma to 1.0 repeatedly makes screen get darker
+ Sys_error when viewing any of the alien models (thirdperson, or if other player sees it)
- When alert sounds, send "hit space to go to alert" to commander.  Prereq: have commander view draw hud messages
+ Creating game on shodan doesn't work?
- Colliding with world problems, when in commander mode
- Software mode: rectangles and lines don't take parent offset into account (I think this is the problem)
X Defection doesn't work correctly (removed)
- When a player demotes himself, update team so the player's leader is right
- Bug: when demoting self, commander slot still taken, can't get another commander
- Add audio/visual order giving feedback
X Ammo and clips disappeared from HUD: gWR.GetWeaponSlot() returning NULL in ammo.cpp, line 1153 (weapon bits aren't coming across?)
X Fix view height tracing
- Land mines become solid when laser pops out, player can get permanently stuck in it
- Fix overwatch entity tracing
O Fix size problem for tech buttons (they are taller than they look)
+ Giving a weld goal gives a location of 0,0,0
- Squad mates that in sight don't get updated, add squad to PVS or send extra position updates for squadmates
O Don't allow researching of the same thing twice
- Be able to give "pick up equipment" orders, by clicking on a dropped weapon
X Be able to give "use" order, by clicking on "useable" (light switches)
O Fix drawing of tech point costs
- Write some kind of test bed for particles to prevent Crinity problems
X Make shotgun smoke look good
- Fix particles to collide with players as well
- Bots (other players) show overwatch when moving (effect doesn't go away?)
O Jump out of top down on the client when map changes
+ Game crashes when map switches (only occasionally, maybe only when leaving viewtest5, ie with buildings?)
- Overwatch view cone is garbage sometimes (wrong sprite frame?)
- When in CS mode, allow order giving anywhere, not just on ground floor
- Commander hierarchy isn't populated by default, only when a player is added to the game after it's been viewed, does it draw anyone
- Fixup observer for commander!

+ High-priority: crash when generating minimap...overwriting end of memory with zero


- Particle system rotations not rotating correctly (MathUtil: AngleMatrix, RotateValuesByVector, VectorRotate)
- Double-check assignment within conditional.  I'm not sure it's picking it up everywhere in UTIL_InSphere
- fmod.dll crash or target_mp3s don't play when maxplayers is 1
- Weirdness eventually leading to crash when switching maps while in commander mode