
Here are some cheat codes and server variables you may find useful.

Cheats 
------
Notes: 
	- sv_cheats must be 1
	- If cheats aren't 1, it will say "unrecognized command", even though it IS a recognized command

spawnhive - As an alien, creates a random new hive for your team
killhive - As an alien, destroys a random hive
addcat x - X where is 1-4.  Adds a random upgrade category, just like you had built an upgrade chamber (1 is defense, 2 used to be offense, 3 is movement, 4 is sensory)
removecat x - Take a guess.
givepoints (bound to F11 by default) - Gives you some points
killall - Kills all players in the game with your current weapon (won't kill teammates unless FF is on via tournament mode)
invul - Toggles invulnerability for yourself.
startgame - Starts game immediately (untested for awhile)
endgame - Ends the current game (as a victory for team one).  You can also type "endgame 1" for the same effect.  If you type "endgame 2", it will be a victory for team 2.
adjustscore x - Changes your current score by x, where x is positive or negative
tooltip lala - Adds a tooltip named "lala".  It will translate this tooltip if found in titles.txt, if possible.
testevent - Plays a "you just got points for killing the enemy event", for testing where the commander can see events (he can't sometimes, when hovering over the void, or inside geometry)
getnumplayers - Reports the current number of human players
startcommandermode - Teleports you near the first command station it finds and starts commander mode.  This will sometimes teleport you inside geometry.
stopcommandermode - Drops you out of command mode, useful when your mouse or the logout button stops working
web - Gives you the effects of having just touched one web.  Use multiple times to simulate touching multiple webs.
setculldistance x - Used by mappers to find the minimum culling distance needed for commander mode on their maps.
setgamma x - Sets the current env_gamma level, where x is a number from 1.0 to 2.0.  This affects all players.
bigdig - All buildings are built instantly, doesn't require manual building
hightech - All research happens nearly instantly
orderself - When commanding and giving orders, this will give the orders to yourself, so you can logout and see how they look on the HUD.  There's no way to stop doing this after you've used this cheat.
editps <name> - Used to edit a particle system
listps - Lists all particle systems in NS and in the map
buildminimap - Generates a new minimap.  Very CPU intensive.  Saved in the ns/sprites/minimaps directory.
crash - Causes NS to crash.
assert - Causes NS to assert
run <name> - Runs a server-side script.  Ignore this until scripting is more in place.
clientrun <name> - Runs a client-side script.  Ignore this until scripting is more in place.
giveupgrade x - Gives an upgrade.  20-22 are the marine armor upgrades, 23-25 are the marine damage upgrades, 28 is jetpacks, 29 is power armor, 31 is faster reinforcements, 33 is motion-tracking, 26 is siege, 27 is stimpack, 30 is distress beacon
removeupgrade x - Like giveupgrade but removes one.
killcs - Kills all command stations.
attackcs - Does a little damage to one command station
alert - Generates a random alert.
parasite - Parasites self (no damage)
paralyze - Paralyzes self (like level 5 ability)
boxes - Draws boxes around all buildables (not sure if this still works).  Effect goes away after a short time.
overflow - Tries to overflow player.
deathmessage <weaponname> - Sends test death message to all.  Example: "deathmessage machinegun", "deathmessage bitegun"
setskin x - Sets the players skin (0 for white marine, 1 for black marine)
redeem - Makes it easier to test redemption

Other useful console commands (these aren't cheats, not yet):

jointeamone - Joins team one, if not on a team
jointeamtwo - Joins team two, if not on a team
spectate - Starts spectating, if not on a team
readyroom - Brings you back to the ready room, if not in the ready room
autoassign - Joins the team with the least players.  If teams are equal, picks a random team.
addbot - Adds a bot to the server, on autoassign.  This is F3 by default.
