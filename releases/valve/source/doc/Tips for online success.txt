  Name: David Groves  
  Location: London  
  Posts: 9     
  http://www.poddle.net
--------------------------------------------------------------------------------

This is a simple list of things that would DRAMATICALLY increase 
the chances of a game becoming a popular online game. Some of these 
things are easier to implement that others, but you will notice the 
most popular games like counterstrike already have most of these 
features. Of course, if you design a really shite game, none of the 
rest of the article is going to be of any use to you whatsoever.






Good netcode. Good examples, Quakeworld + Qizmo. Low bandwidth, 
consistent, little warpyness, relatively kind on packetloss and 
high latency (which are impossible to deal with), doesn't 
excessively trust the client. Bad examples, Quake3, unplayable on 
ISDN, certain CS versions, trusted the client far too much, making 
for trivial exploits. Trust me game developers, hire Zibbo and 
Perkele (zibbo@udpsoft.com) to sort out your netcode, I doubt you 
will regret it. 



Include a 'competition' mode WITH the game. The best examples of 
this are kteams for Quakeworld, or OSP for Quake3. This allows 
organised teams of people to play easier. Notably CS hasn't done 
this. Perhaps if they did, the competitive scene in it would be 
even larger than it is. 



Multilevel admin support. Administrator privileges on a server 
shouldn't be all or nothing. Almost all games ship with it like 
this at the moment, with the 'rcon' system. I don't want to give 
some people administrator rights to kick idiots, and maybe change 
the map if its a 20v20 map and 4 people are playing, and also give 
them the rights to shutdown the server, and maybe poke around on 
the computer its hosted on (rcon dir in Q3 springs to mind). 



Voicecomms. Integrate voicecomms into your games. That is the 
ONLY way that public servers are going to see voicecomms in use, 
and that is the ONLY way that you will see real teamplay on public 
servers. I'd really like to see a project get going to write a BSD 
licensed bit of code for windows clients, and windows/*nix servers 
that just allows this to be a "bolt on" job for game developers. 
Anyone also interested in doing this, please mail me.


This also lets you impose things, like stopping dead players 
from talking to players. This is GOOD. Remember the dedicated lamer 
clans will cheat by using an external voicecomms programs anyway, 
but this at least helps. 



Be LAN friendly. Yes, this is mainly you blizzard. Always allow 
people to just type in an IP address to connect to another server. 
Some large lan events VLAN networks into several segments, mainly 
for management reasons, and to limit exploits (accidental, like 
lan clients running DHCP servers, or intentional). Since most "broadcast" 
methods of finding games won't cross these boundaries, just let 
people type in IP addresses or hostnames somewhere please.


Also, and Tribes2 and Dynamix are being shouted at here, don't 
make assumptions based on the netmask about clients being on LAN or 
the Internet, thanks. 



Be mod friendly. Keep a non-profit EULA if you want, but release 
mod tools for your game. Go the undocumented route if you want. 
Just look at halflife and counterstrike to see the benefits this 
can bring. Well played id software for making this common in the 
first place. 



Cheats. We know it's not possible to make a cheat free game when 
you have to put any trust in the client at all, and we know its not 
possible to make a playable game if you don't put at least some 
trust in the client. But it is possible to make the cheaters life 
harder by releasing constant patches that just make them have to 
work harder with a disassembler to insert the correct hooks, or 
crack the appropriate part of the network code.



Maybe the right answer to this is a patch a day, with the 
compile run through an object code obfuscator with a different seed 
each day. This does need a good update mechanism though, see 
below, and may well be fatally flawed if the obfuscator is predictable. This combined with normal bugfixing patches that change the code logic should at least make things very difficult though.





Patches. As mentioned above, if you are going to have frequent 
patches, you need a good auto update mechanism. You also need a good 
non-auto update mechanism. Heres why.


Without a good auto update system, people won't have the latest 
version, and having to patch up to play deters the casual 
gamer.


Without a good non-auto update people, it is hard for people 
without internet connectivity, or temporally without internet 
connectivity (ie. at a lan), to update.


Ideally the auto update mechanism for the clients is just to ask 
them if they want to update, and then for it to just work. It 
should be fast too, so maybe keep deltas from every version to the 
latest version on your update server, rather than patching through 
30 versions to get to the latest.


Ideally the non-auto update mechanism should be a single patch 
that takes any version to the latest version. It's less of an issue 
if this has to go through every revision to get to it though.




Server patching. Nobody seems to have done auto-server patching 
yet. Lots of people do auto-client patching. This to me seems 
equally important. See the section on patch security though. Maybe 
with FPS like games you can check for an update at a map change, 
and with other games, maybe have them all shutdown for update at a 
specific time (and broadcast warnings about it before it happens, 
like unix computers being shutdown). Ideally, be like irssi, the 
IRC client (www.irssi.org), and don't need to be shutdown at all, 
(but don't be like irssi and get your configure script trojaned 
:). 



Server browsing. Either do it right, or leave it to someone 
else, don't do a half assed job. A good example of an in game server 
browser is UT. A bad example is Quake3. A good example of leave it 
to someone else is Quakeworld (and the then Quakespy). 



GUID's. Being able to see other players GUID's, (usually a hash 
on the CD key) is important. Particularly if like most FPS games 
you don't have accounts to get names. This lets you see if the 
person you are playing is REALLY the person they claim to be, and 
stops people pretending to be someone else and behaving like a 
lamer to get a bad reputation for that person. 



Secure updates. Please sign your updates in a secure manner. You 
have an excellent method of distributing your key, just put it on 
the CD that the game comes on. 



CD key authentication. This is needed to prevent piracy, 
and thats good. The client --> CD key hash --> server --> 
authserver is the correct approach though. Also, PLEASE make your 
auth servers reliable, nothing pisses people off more than being 
unable to play a game because you or your ISP have ed up. (Hello Blizzard !). 



*nix servers. Don't be like Medal of Honour. Lots of server admins like 
to run *nix servers. Why take a game based on the Q3 engine that 
has a wonderful linux port, and just break it. Please design your 
games so at least the server can run on linux. Ideally have it able 
to run on as much as possible. I'm not sure of the current state of 
cross-compiling, but this seems to be an ideal way to get out other 
platforms too. Imagine a IBM Bigass S/390, or a Sun E15k system running 19247819741 of 
your servers, doesn't that appeal ? 



No backdoors. Just NO backdoors. Bad id software. 



Fix your bugs. A crash bug needs a patch, no matter how rare the 
crash is. Example of unfixed crash bugs in games are Quake1, and 
the teleporter at the start of e4m8 (RJ onto the right hand side of 
it). A blatant exploit bug that allows people to spoil the game 
also needs quick fixing. CnC renegade and the beacon your own base 
even though friendly fire is off if you quit the game after 
dropping the beacon bug, this BADLY needs fixing.





Listen to the community. Anything the game lets you do shouldn't 
be classed as cheating. I'm thinking mainly counterstrike here. In 
counterstrike, some people decided to make standing on each others 
heads to make a tower of players to boost someone up to a normally 
unaccessible place against the 'local rules'. Imo, if the game 
allows you to do it, it isn't cheating. If this is to be considered 
cheating, the game should be patched to stop it.


Also, some bugs are GOOD. Remember rocket jumping wasn't planned 
in Quake1, it was an 'incidental feature'. Neither was bunnyhopping 
in Quakeworld. In my opinion both of these add to the game. If 
you ARE going to change something because you don't like it, but 
missed it in QC, do it EARLY. Bad examples of this were the 
removal of bunnyhopping from Counterstrike, and the removal of through 
floors damage in Quake3, both of which should have been done 
FAR earlier than they were (or just plain left alone).


The same applies to graphics settings. RTCW is a good example of 
the 'right' approach to this, but I don't like the way punkbuster 
and it allow different server operators to declare different 
standards. This leads to situations like in Quakeworld, where 
different countries play different rules, which leads to all kinds 
of problems in international play. This isn't like Amercian 
Baseball, and the designated hitter rule, this is like different 
numbers of players on each teams, totally different maps, and 
differences in your ability to hurt your teammates or not.


Nothing is wrong with having softcore modes and hardcore modes, 
as long as its clear that the competitive players should be using 
the hardcore mode, and you are only changing the rules between 
modes, not things like the physics or the map layout. ie. this 
shouldn't be vanilla Quake3 and CPMA Quake3, which I would class as 
different (although similar) games. 



Dedicated servers. Please work hard at optimising these so they 
need as little memory and CPU as possible. This will make the 
people that host your servers happy. I really am quite stunned when 
I see the amount of resources say Quake3 consumes compared to 
Quakeworld. I can understand it from the clients and flashy 
graphics point of view, but I'm not sure why they require it on the 
server side too. 



Dedicated server hooks. I would love to see a standard API for 
controlling programs to interface with dedicated servers, to add 
bans, shut them down, change the mode they are running in etc ... 
At the very least, please make your win32 servers simple console 
apps, and not have rubbish wrappers around them to make them look 
pretty (or at least have a runtime flag that gets rid of that 
rubbish). This makes manually hooking into your server far 
easier.


Also, if you write a *nix dedicated server that needs X, you 
should be shot. (I've forgotten which developer did this). 



Dedicated servers that don't require the game to run. This 
allows ISP's and the like that wouldn't go and buy the game to run 
the server. Halfife GOOD, Quake3 BAD. 



Console. id software introduced the console, and the console 
was good. However, the console should only be a quick way for 
experienced people to operate things. You shouldn't ever need to 
drop to the console for anything. Every single graphics tweak, 
sound tweak or whatever that the game developers consider "legal" 
should be in the menus somewhere, preferably with a tooltip or 
other explanation saying what they do.


Also, please use the standard console key, and don't need people 
to run the program with some fiddly command line option to get at 
it. 



International keyboard support. This is getting better, but 
Quake1 had a total disregard for it, as did Allegiance (a MS game 
that died due to several bits of mismanagement that reading this 
webpage may have solved). 



Portable settings. A player should be able to save all his 
settings in one file, and be able to load all these settings on 
another computer. He should have to fart about with what settings 
are in the autoexec config, and what are in playername.cfg, and 
what are in the mods directory config.cfg etc ... It should be a 
simple option to "save all my settings".


Of course some things like mouse drivers affecting sensitivity 
are impossible to deal with, but you should make your best effort 
at this. 



Autodownloading of custom content. Without this custom content 
doesn't get used. It's pretty much as simple as that. Ideally have 
a field that is part of the custom content that is "download 
locations", and have the client fetch (by http or ftp) from these 
locations if possible in preference to fetching directly from the 
server (to save on bandwidth on the server). If this isn't 
possible, have a tuneable parameter as to how much bandwidth the 
server will use for providing custom content to clients (don't have 
it default to something far too low, like Quake3 does). 



Multicast spectator tools. Like HLTV. Ideally actually using 
proper multicasting when the internet in general is up to it. These 
are essential if you want your game to get a good following as a 
'spectator sport', in addition to having people play it. Possibly 
have the viewer for this not require the game, so people can 
download it, watch championship level games, and want to buy the 
game to play it themselves. Like the Tennis effect in the UK 
everytime Wimbledon is on the TV, for the month afterwards, all the 
public courts are full, tennis racquets sell 50% of year round 
sales at this time, and then everyone forgets it till next year. 



Demo recording. It should be easy to record demos from a players 
point of view, of what they did that game, for later playback. 
Ideally, the server should also be able to record server side demos 
of what every player did in the entire game. Please look at 
compressing these demos as tightly as possible, look at the file size differences in .qwd and 
.qwz in Quakeworld/Qizmo for an example of what happens when you 
don't. 