
- Fix up prediction errors on all weapons
	O Wait for Yahn for a bit
	O Weird holster/deploy problem
	O Shotgun not working
	O Claws not working
	- Knife slash sounds not predicted
	O client version of weapon has 10 bullets in MG and shotgun clip even when out of ammo, so effect fires anyways.  Figure out where clip is propagated

O Fix bug where AvHTeam::GetHasTeamLost() doesn't take into account reinforcements

O Take out code that lets everyone have all weapons

O Remove error logging to hl.log (hl_weapons.cpp)

+ Switching to welder crashes the game (iuser3 assert in gamerules line 2153)

- Verify: Set starting points for marines correctly (when 2+ marines and afer a map restart)

+ Make sure welding works currently (it doesn't.  this was caused when changing solidity states for welding, trying to fix that bug)

- Remove glock, glock ammo and "b" bind

- Fix all weapons
	+ Machine gun broken
	+ Shot gun broken
	- Welder broken?
	+ Fix weapons so they are in the right place in the HUD
	+ Fix up weapon_ text files
	+ Fix claws

- Tweak rate of fire and damage for all weapons so the weapons feel better and have a tiny hope of balance

- Fix turrets
	- They are only thinking every once in awhile!

- Fix siege turrets

- When level resets
	+ Remove all tripmines
	- Reset doors if possible
	+ Reset command stations

+ New model for hive

+ Remove all dead bodies

- Add hive mind effect for all aliens
	- Server checks if aliens are visible, maintains list of effects
	- Propagates effects to client
	- Client draws effects
	- Make general enough to use for commander LOS, including showing up for friendlies when target of an order

- Alien weapons 
	O Make sure alien flight works again (it's just resetting some pev->iuser variable I think)
	- Fix the existing alien weapons so they all do damage
		+ Spores
		- Make spit do something
		- Make ensnare do something
	X Alien adrenaline when hitting each other
	O At least one weapon for each alien, two weapons for a couple

- Fix turrets again, they're hitting the world or the floor, though their effects look right (sparks, ricochet).  Their construction complete is weird and not right.

O Alien flashlight bug

O Allow morphing back to level 1
	O Fix up alien menu so there aren't unused nodes

O Don't deduct points or evolve if player tries to evolve to current lifeform

O Make sure jetpack is given on upgrade (it's getting lost when researched)

O Make sure aliens respawn as level 1

O Show current resources for aliens!

O Remove currently unused nodes off alien pie menu

O Mines are solid unless they are in landmine mode, then they become solid_not
	- Changed to be solid_trigger always (at least until this is fixed for real)

+ Hives make wound sounds and death sound

+ Allow mines to be bought and dropped from commander mode

- Use the above system for the commander LOS
	- When an alien is within range of a soldier (not commander!), add it to the LOS for the commander

- Must be able to kill camera tower and turret, they are marked as damage_none or something

- Add all teammates to TeamHierarchy

- Add responsive order-giving for commander
	- 

O Fix observer mode
	+ Looking up and down
	O Observing commander
	+ Observing enemy team

- Make sure teams are set correctly for turrets, siege turrets, mines
	- Test to make sure they give resources properly on kill
		
- Add real research times for upgrades
	- Add cheat that makes all research instant (if mp_testing is on)
	- Make the tech tree real

- Commander power-ups
	- Adrenaline
		- Nice effects in radius
	- Another area effect?
	- Shield
	- Blaze of glory (only if you have a reinforcement available?)

O Add simple hit effect for siege turret

- Using command station heals the player (feature?)

- Make another version of ns_solar fit for marines vs. marines, we could try that too

- Make hives obvious goals.  Paint on mini-map

- Need mini-map!

- Make sure marines die really really easily.  Just a shot or two from any alien (increase spit gun damage)

+ Add alien "machine gun" (shoots spikes)

O Make sure camera tower can be destroyed and gives points
	+ Make info_gameplay super general and customizable

O Allow destruction of command station?

+ Put walls over tops of command stations so they can't be landed on.  Otherwise, when the player logs out, he falls onto command station and logs back in (infinitely)

O Remove items off menu that aren't functional (nuke, flamer, infravision)?

O Get phase gates to link (maybe they link to the next/last one built?


- Tech tree
Routes: 
	mass, expand: camera towers, but some defense or reinforcements
	quick attack (rush): reinforcements, ammo, weapon upgrades, command
	slow n' steady: move to a hive location, kill the hive if there, stake it out with turrets if not

Ideas:
	maybe only one weapon upgrade, but make it big (the really obvious one)
	jetpacks must be upgrade, can't be at bottom level, must take at least one other to get to it (or have some obvious way for aliens to see it coming)
	reinforcements must be researched first?
	siege must be researched
	don't have tech that does multiple things, ie damage, AND allows HMGs.  this is confusing, makes the tech hard to name and makes strategy less clear (or painfully simple)



